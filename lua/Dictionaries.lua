package.path        = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";
local MCASTMUX_HOME = os.getenv("MCASTMUX_HOME");
local lscript_path  = MCASTMUX_HOME.."/lua"
local JSONf         = assert(loadfile(lscript_path.."/JSON.lua"));
local JSONf         = JSONf()
local zapi          = require("zapi");
                      require("LuaXml")
local XML           = xml
local soap          = require("CMDBuildSOAPAPI")

Dictionaries={
  ["AboutME"]={
    _MODULE         = "Dictionaries",
    _VERSION        = "Dictionaries v0.1.0-rc1",
    _AUTHOR         = "Pavel Kraynyukhov",
    _AUTHOR_MODULE  = "Alexandr Mikhailenko (he also) FlashHacker",
    _URL            = "https://bitbucket.org/enlab/Dictionaries",
    _MAIL           = "flashhacker1988@gmail.com",
    _COPYRIGHT      = "Design Bureau of Industrial Communications LTD, 2015. All rights reserved.",
    _LICENSE        = [[
      MIT LICENSE
      Copyright (c) 2015 Mikhailenko Alexandr Konstantinovich (a.k.a) Alex M.A.K
      Permission is hereby granted, free of charge, to any person obtaining a
      copy of this software and associated documentation files (the
      "Software"), to deal in the Software without restriction, including
      without limitation the rights to use, copy, modify, merge, publish,
      distribute, sublicense, and/or sell copies of the Software, and to
      permit persons to whom the Software is furnished to do so, subject to
      the following conditions:
      The above copyright notice and this permission notice shall be included
      in all copies or substantial portions of the Software.
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
      ]],
    _DESCRIPTION    = [[
      Dictionaries from CMDBuild for LLP "CB PROMSVYAZ"
      How to use:
        local dict=require("Dictionaries")
        dict.load(LOGIN,PASS,URL)
    ]]
  },
  ["GetSOAPReturn"]=function(xmlresp)
    local xmltab=XML.eval(xmlresp)
    local result=xmltab:find("ns2:return")
    if result and type(result) == "table" then return result[1] end
    return nil
  end,
  ["GetSOAPCards"]=function(xmlresp)
    local xmltab=XML.eval(xmlresp)
    local result=xmltab:find("ns2:cards")
    return result
  end,
  ["getLookupListResponse"]=function(xmlresp)
    local xmltab=XML.eval(xmlresp)
    local result=xmltab:find("ns2:getLookupListResponse")
    if result and type(result) == "table" then return result[1] end
    return nil
  end,
  ["isin"]=function(str,array)
    local theResult=false
    for i,v in ipairs(array) do if(v == str) then theResult=true end end
    return theResult
  end,
  ["xml_escape"]=function(value) return value:gsub("&","&amp;"):gsub([["]],"&quot;"):gsub("'","&apos;"):gsub("<","&lt;"):gsub(">","&gt;"); end,
  ["List"]={"EquipmentClass","ValueTypes","host_status","LOC_TYPE","snmp_available",
                        "zItemStatus","jmx_available","ipmi_privilege","data_type","ipmi_authtype",
                        "ztriggerState","valuemaps","EquipmentStatus","snmpv3_securitylevel",
                        "snmpv3_authprotocol","ztriggerType","ztriggerValue","units","HostStatus",
                        "flags","maintenance_type","delta","ipmi_available","maintenance_status",
                        "snmpv3_privprotocol","ItemType","zItemState","auth_type","BooleanEnum",
                        "Severity","MacroType"
  },
  ["lookups"]={},
  ["TColumn2Lookup"]={},
  ["load"]=function(WS_ADMIN,WS_PASS,WS_URL)
    table.foreach(Dictionaries.List,function(key,value)
      local xmltext=soap.getLookupList(WS_ADMIN, WS_PASS,value)
      local xmlresp=soap.retriveMessage(soap.Send(WS_URL,xmltext))
      local retval=Dictionaries.getLookupListResponse(xmlresp)
      local result=(XML.eval(xmlresp))
      for i=1,table.maxn(result) do
        for j=1,table.maxn(result[i]) do
          for k=1,table.maxn(result[i][j]) do
            if Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]] then
              Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]][tostring(result[i][j][k]:find("ns2:code")[1])]=result[i][j][k]:find("ns2:id")[1]
            else
              Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]]={}
              Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]][tostring(result[i][j][k]:find("ns2:code")[1])]=result[i][j][k]:find("ns2:id")[1]
            end
          end
        end
      end
    end)
    Dictionaries.TColumn2Lookup["templates"]={}
    Dictionaries.TColumn2Lookup["Hosts"]={}
    Dictionaries.TColumn2Lookup["HostTypes"]={}
    Dictionaries.TColumn2Lookup["ztriggers"]={}
    Dictionaries.TColumn2Lookup["zItems"]={}
    Dictionaries.TColumn2Lookup["zUsermacro"]={}
    Dictionaries.TColumn2Lookup.templates["ipmi_authtype"]="ipmi_authtype"
    Dictionaries.TColumn2Lookup.templates["flags"]="flags"
    Dictionaries.TColumn2Lookup.templates["status"]="host_status"
    Dictionaries.TColumn2Lookup.templates["snmp_available"]="snmp_available"
    Dictionaries.TColumn2Lookup.templates["maintenance_type"]="maintenance_type"
    Dictionaries.TColumn2Lookup.templates["maintenance_status"]="maintenance_status"
    Dictionaries.TColumn2Lookup.templates["jmx_available"]="jmx_available"
    Dictionaries.TColumn2Lookup.templates["ipmi_privilege"]="ipmi_privilege"
    Dictionaries.TColumn2Lookup.templates["ipmi_available"]="ipmi_available"
    Dictionaries.TColumn2Lookup.HostTypes=Dictionaries.TColumn2Lookup.templates;
    Dictionaries.TColumn2Lookup.Hosts=Dictionaries.TColumn2Lookup.templates;
    Dictionaries.TColumn2Lookup.Hosts["OpStatus"]="HostStatus"
    Dictionaries.TColumn2Lookup.ztriggers["type"]="ztriggerType"
    Dictionaries.TColumn2Lookup.ztriggers["flags"]="flags"
    Dictionaries.TColumn2Lookup.ztriggers["state"]="ztriggerState"
    Dictionaries.TColumn2Lookup.ztriggers["status"]="zItemStatus"
    Dictionaries.TColumn2Lookup.ztriggers["value"]="ztriggerValue"
    Dictionaries.TColumn2Lookup.ztriggers["priority"]="Severity"
    Dictionaries.TColumn2Lookup.zItems["type"]="ItemType"
    Dictionaries.TColumn2Lookup.zItems["value_type"]="ValueTypes"
    Dictionaries.TColumn2Lookup.zItems["authtype"]="auth_type"
    Dictionaries.TColumn2Lookup.zItems["data_type"]="data_type"
    Dictionaries.TColumn2Lookup.zItems["delta"]="delta"
    Dictionaries.TColumn2Lookup.zItems["flags"]="flags"
    Dictionaries.TColumn2Lookup.zItems["multiplier"]="BooleanEnum"
    Dictionaries.TColumn2Lookup.zItems["snmpv3_authprotocol"]="snmpv3_authprotocol"
    Dictionaries.TColumn2Lookup.zItems["snmpv3_privprotocol"]="snmpv3_privprotocol"
    Dictionaries.TColumn2Lookup.zItems["snmpv3_securitylevel"]="snmpv3_securitylevel"
    Dictionaries.TColumn2Lookup.zItems["state"]="zItemState"
    Dictionaries.TColumn2Lookup.zItems["status"]="zItemStatus"
    Dictionaries.TColumn2Lookup.zItems["valuemapid"]="valuemaps"
    Dictionaries.TColumn2Lookup.zItems["units"]="units"
    Dictionaries.TColumn2Lookup.zUsermacro["type"]="MacroType"
    print("finished")
  end,
  ["isempty"]=function(s) return s == nil or s == ''end,
  ["getLookUpId"]=function(t,c)
    if t and not isempty(tostring(c)) then
      local retval=Dictionaries.lookups[t][tonumber(c)]
      if(retval) then return retval else return error("Dictionary is incomplete for lookup: "..t.." with the code: "..c); end
    else
      return error("Dictionary is incomplete or nil reference being requested for lookup: "..t.." with the code: "..c);
    end
  end
}

return Dictionaries